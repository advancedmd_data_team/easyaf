package main

import (
	"log"

	"github.com/jessevdk/go-flags"
	"gitlab.com/advancedmd_data_team/easyaf/internal/commands"
)


func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	parser := flags.NewParser(&commands.EasyAF, flags.Default)
	_, err := parser.Parse()
	if err != nil {
		if flags.WroteHelp(err) {
			return
		}
		log.Fatal(err)
	}

}
