# Easy Airflow

A Tool designed to make managing airflow pipelines easier than the WebUI or the airflow CLI.

# Usage Overview
```
Usage:
  easyaf [OPTIONS] <list | pause | trigger>

Application Options:
      --status=[active|paused|all]               Filter by status. Defaults to
                                                 active (default: active)
  -a, --author=                                  Filter dags to those with this
                                                 author
  -t, --tag=                                    Filter results on a tag
  -s, --last-run=[failed|success|running|queued] Filter results on last run
                                                 status
      --no-earlier=                              Filter results to only those
                                                 whose last run is after date.
                                                 Date should be RFC3339 format.
      --no-later=                                Filter results to only those
                                                 whose last run is before date.
                                                 Date should be RFC3339 format.
  -u, --username=                                username to use to connect to
                                                 airflow [$AIRFLOW_USERNAME]
      --password=                                password to connect to airflow
                                                 [$AIRFLOW_PASS]
  -H, --host=                                    Host name for airflow instance
                                                 [$AIRFLOW_URL]

Help Options:
  -h, --help                                     Show this help message

Available commands:
  list
  pause
  trigger
```

`easyaf` contains three subcommands: `list`, `pause`, and `trigger`. To each of these subocommands
you are able to supply a slew of global options. These options can be passed in any order. This
means that the following two commands are equivalent:

```bash
easyaf --status=puased list
easyaf list --status=puased
```

## Global Options

There are two kinds of global options that can be passed: Filtering criteria, and connection criteria.
The connection criteria are used to gain a connection to the airflow webserver hosting the airflow
REST api. Filtering criteria are all applied in an AND fashion, except where noted otherwise. Therfore
using the `-t` and `-s` flag will filter the operation (`list`, `trigger`, or `pause`) to those
pipelines that meet both criteria.

### --status

Filters the operation to only apply to those pipelines that are of a certain status. Only will
accept `active`, `paused` or `all`. Defaults to `active`.

### -a, --author

Filters the operation to only apply to those pipelines with the author/owner supplied. Because
Airflow allows multiple owners for each pipeline, this option can be specified multiple times.
If it is specified multiple times, each will be applied in an AND fashion, meaning only pipelines
with all supplied values as owners will be selected.

### -t, --tag

Filters the operation to only apply to those pipelines with the supplied tag. Can be specified
multiple times. If supplied multiple times only those pipelines with ALL supplied tags will
be selected.

### -s, --last-run

Filters the operation to only apply to those pipelines with a last run status of <status>. Valid
values are `running`, `queued`, `failed`, and `success`. Can be given multiple times.
This is the only operation where, if specified multiple times, it is treated as an OR. This is
because a dag run cannot have two different statuses.

### --no-earlier

Filters the operation to pipeliens with a last run start date of no earlier than the supplied
date. The Date must be RFC3339 format. This can be generated easily using the `date` command
with the option `-Iseconds`.

### --no-later

Filters the operation to pipeliens with a last run start date of no later than the supplied
date. The Date must be RFC3339 format. This can be generated easily using the `date` command
with the option `-Iseconds`.

### -u, --username

Username to use to connect to the REST api. If not supplied, will dafault to $AIRFLOW_USERNAME

### --password

Password to use to connect to the REST api. If not supplied, will default to $AIRFLOW_PASS

### -H, --host

Host to connect to the REST API. If not supplied, will default to $AIRFLOW_URL

## Subcommands

### List
```
Usage:
  easyaf [OPTIONS] list

Application Options:
      --status=[active|paused|all]               Filter by status. Defaults to
                                                 active (default: active)
  -a, --author=                                  Filter dags to those with this
                                                 author
  -t, --tags=                                    Filter results on a tag
  -s, --last-run=[failed|success|running|queued] Filter results on last run
                                                 status
      --no-earlier=                              Filter results to only those
                                                 whose last run is after date.
                                                 Date should be RFC3339 format.
      --no-later=                                Filter results to only those
                                                 whose last run is before date.
                                                 Date should be RFC3339 format.
  -u, --username=                                username to use to connect to
                                                 airflow [$AIRFLOW_USERNAME]
      --password=                                password to connect to airflow
                                                 [$AIRFLOW_PASS]
  -H, --host=                                    Host name for airflow instance
                                                 [$AIRFLOW_URL]

Help Options:
  -h, --help                                     Show this help message
```

List will list out all pipelines that meet the criteria. This is useful for
using as a dry run to see what pipelines other operations will affect.

### Trigger
```
Usage:
  easyaf [OPTIONS] trigger [trigger-OPTIONS]

Application Options:
      --status=[active|paused|all]               Filter by status. Defaults to
                                                 active (default: active)
  -a, --author=                                  Filter dags to those with this
                                                 author
  -t, --tags=                                    Filter results on a tag
  -s, --last-run=[failed|success|running|queued] Filter results on last run
                                                 status
      --no-earlier=                              Filter results to only those
                                                 whose last run is after date.
                                                 Date should be RFC3339 format.
      --no-later=                                Filter results to only those
                                                 whose last run is before date.
                                                 Date should be RFC3339 format.
  -u, --username=                                username to use to connect to
                                                 airflow [$AIRFLOW_USERNAME]
      --password=                                password to connect to airflow
                                                 [$AIRFLOW_PASS]
  -H, --host=                                    Host name for airflow instance
                                                 [$AIRFLOW_URL]

Help Options:
  -h, --help                                     Show this help message

[trigger command options]
          --dag-id=                              DagId to trigger, can be
                                                 specified multiple times.
                                                 Filters are ignored
```

Triggers the pipelines specified by the filter, or those listed by `--dag-id`.
The `--dag-id` option is only available after having specified the trigger
command. It also supersedes any filters applied, meaning filters will be
ignored.

### Puase
```
Usage:
  easyaf [OPTIONS] pause [pause-OPTIONS]

Application Options:
      --status=[active|paused|all]               Filter by status. Defaults to
                                                 active (default: active)
  -a, --author=                                  Filter dags to those with this
                                                 author
  -t, --tags=                                    Filter results on a tag
  -s, --last-run=[failed|success|running|queued] Filter results on last run
                                                 status
      --no-earlier=                              Filter results to only those
                                                 whose last run is after date.
                                                 Date should be RFC3339 format.
      --no-later=                                Filter results to only those
                                                 whose last run is before date.
                                                 Date should be RFC3339 format.
  -u, --username=                                username to use to connect to
                                                 airflow [$AIRFLOW_USERNAME]
      --password=                                password to connect to airflow
                                                 [$AIRFLOW_PASS]
  -H, --host=                                    Host name for airflow instance
                                                 [$AIRFLOW_URL]

Help Options:
  -h, --help                                     Show this help message

[pause command options]
          --dag-id=                              DagId to pause, can be
                                                 specified multiple times.
                                                 Filters are ignored
      -i, --invert

```
Pauses the pipelines specified by the filter, or those listed by `--dag-id`.
The `--dag-id` option is only available after having specified the pause 
command. It also supersedes any filters applied, meaning filters will be
ignored. You can pass in the `-i` or `--invert` flag to unpause the 
specified pipelines.

## Examples
```bash
# To list all pipelines by John Doe you can do:
easyaf -a "John Doe" list
# To Trigger all pipelines that have a last status of failed
easyaf -s failed trigger
# To pause all pipelines that were last run between last tuesday and last friday with a status of failed
easyaf -s failed --no-earlier=$(date -Iseconds --date "last tuesday") --no-later=$(date -Iseconds --date "last friday") pause
```
