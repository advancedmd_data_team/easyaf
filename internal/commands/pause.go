package commands

import (
	"log"

	"gitlab.com/advancedmd_data_team/easyaf/internal/airflow"
)

type PauseOpts struct {
	DagIds []string `long:"dag-id" description:"DagId to pause, can be specified multiple times. Filters are ignored"`
	Invert bool     `short:"i" long:"invert" defaut:"false"`
}

func (o PauseOpts) Execute(args []string) error {

	auth := airflow.InitAuth(EasyAF.Username, EasyAF.Password)
	client := airflow.InitClientData(EasyAF.Host, auth)

	var dagsIds []string

	// If DagIds are specified just trigger those ones
	if len(o.DagIds) > 0 {
		dagsIds = o.DagIds
	} else {
		dags, err := FilterDagsByOpts(client)
		if err != nil {
			return err
		}
		for _, dag := range dags {
			dagsIds = append(dagsIds, dag.DagId)
		}
	}

	// Trigger them dags
	log.Printf("Pause is: %t", !o.Invert)
	results, err := client.UpdateDags(dagsIds, !o.Invert)
	if err != nil {
		log.Fatal(err)
	}
	displayDags(results)
	return nil
}
