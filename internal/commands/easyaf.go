package commands

type EasyAFOpts struct {
	Status        string   `long:"status" description:"Filter by status. Defaults to active" choice:"active" choice:"paused" choice:"all" default:"active"`
	Author        []string `short:"a" long:"author" description:"Filter dags to those with this author"`
	Tags          []string `short:"t" long:"tag" description:"Filter results on a tag"`
	LastRunStatus []string `short:"s" long:"last-run" description:"Filter results on last run status" choice:"failed" choice:"success" choice:"running" choice:"queued"`
	NoEarlierThan string   `long:"no-earlier" description:"Filter results to only those whose last run is after date. Date should be RFC3339 format."`
	NoLaterThan   string   `long:"no-later" description:"Filter results to only those whose last run is before date. Date should be RFC3339 format."`

	Username string `short:"u" long:"username" description:"username to use to connect to airflow" env:"AIRFLOW_USERNAME"`
	Password string `long:"password" description:"password to connect to airflow" env:"AIRFLOW_PASS"`
	Host     string `short:"H" long:"host" description:"Host name for airflow instance" env:"AIRFLOW_URL"`

	QueryCommend QueryOpts `command:"list"`
	TriggerCommand TriggerOpts `command:"trigger"`
	PauseCommand PauseOpts `command:"pause"`
}

var EasyAF = EasyAFOpts{}
