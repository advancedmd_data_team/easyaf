// Functions for querying the list of pipelines
package commands

import (
	"log"
	"os"
	"text/template"
	"time"

	"gitlab.com/advancedmd_data_team/easyaf/internal/airflow"
)

type QueryOpts struct {
}

// Query the dags, apply filters and then format them with information in a pretty way
func (o QueryOpts) Execute(args []string) error {

	auth := airflow.InitAuth(EasyAF.Username, EasyAF.Password)
	client := airflow.InitClientData(EasyAF.Host, auth)

	dags, err := FilterDagsByOpts(client)
	if err != nil {
		return err
	}

	displayDags(dags)
	return nil
}

// Filters dags using the options passed to easyopt
func FilterDagsByOpts(client airflow.ClientData) ([]airflow.Dag, error) {
	dags, err := client.FetchAllDags()
	if err != nil {
		return nil, err
	}

	dags = filterDags(dags)

	var dagRuns map[string]airflow.DagRun
	if len(EasyAF.LastRunStatus) != 0 || EasyAF.NoEarlierThan != "" || EasyAF.NoLaterThan != "" {
		// Then we need to fetch last run data and filter on it. In this case we will OR them
		// since each Run can only hold one status
		dagRuns, err = client.FetchLastDagRuns(dags)
		if err != nil {
			return nil, err
		}
	}
	if len(EasyAF.LastRunStatus) != 0 && dagRuns != nil {
		dags = filterDagsByStatus(dags, dagRuns)
	}
	if (EasyAF.NoEarlierThan != "" || EasyAF.NoLaterThan != "") && dagRuns != nil {
		dags, err = filterDagsByDate(dags, dagRuns)
		if err != nil {
			return nil, err
		}
	}
	return dags, nil
}

func displayDags(dags []airflow.Dag) {
	templStr := `|{{printf "%-50s" "DAG_ID" }} | {{ printf "%-20s" "AUTHOR" }} | {{printf "%-10s" "ACTIVE" }} |
-----------------------------------------------------------------------------------------
{{ range .}}|{{ .DagId | printf "%-50s"}} | {{ range .Owners }}{{ printf "%-20s" . }}{{end}} | {{not .IsPaused | printf "%-10t" }} |
{{end}}
`

	templ, err := template.New("queryOutput").Parse(templStr)

	if err != nil {
		log.Fatal(err)
	}

	err = templ.Execute(os.Stdout, dags)
	if err != nil {
		log.Fatal(err)
	}

}

// Filters Dags based off of information available in the Dag struct
func filterDags(dags []airflow.Dag) []airflow.Dag {
	// Filter on tags
	var filtered []airflow.Dag
	for _, dag := range dags {
		if hasTags(EasyAF.Tags, dag) && hasAuthors(EasyAF.Author, dag) && hasStatus(EasyAF.Status, dag) {
			filtered = append(filtered, dag)
		}
	}
	return filtered
}

func hasStatus(status string, dag airflow.Dag) bool {
	if status == "all" {
		return true
	}
	var wantPaused = status == "paused"
	return dag.IsPaused == wantPaused
}

func hasAuthors(authors []string, dag airflow.Dag) bool {
	if len(authors) == 0 {
		return true
	}
	var allAuthors []bool
	for _, author := range authors {
		containsAuthor := false
		for _, dauth := range dag.Owners {
			if author == dauth {
				containsAuthor = true
				break
			}
		}
		allAuthors = append(allAuthors, containsAuthor)
	}
	for _, val := range allAuthors {
		if !val {
			return false
		}
	}
	return true
}

func hasTags(tags []string, dag airflow.Dag) bool {
	if len(tags) == 0 {
		return true
	}
	var allTags []bool
	for _, tag := range tags {
		containsTag := false
		for _, dtag := range dag.Tags {
			if tag == dtag.Name {
				containsTag = true
				break
			}
		}
		allTags = append(allTags, containsTag)
	}
	for _, v := range allTags {
		if !v {
			return false
		}
	}
	return true
}

func filterDagsByStatus(dags []airflow.Dag, dagRuns map[string]airflow.DagRun) []airflow.Dag {
	var filtered []airflow.Dag
	for _, status := range EasyAF.LastRunStatus {
		for _, dag := range dags {
			if dagRuns[dag.DagId].State == status {
				filtered = append(filtered, dag)
			}
		}
	}
	return filtered
}

func filterDagsByDate(dags []airflow.Dag, dagRuns map[string]airflow.DagRun) ([]airflow.Dag, error) {
	var start = new(time.Time)
	var end = new(time.Time)
	TIME_BEFORE_TIME := new(time.Time)
	var err error

	err = parse_time(EasyAF.NoEarlierThan, start)
	if err != nil {
		return nil, err
	}

	err = parse_time(EasyAF.NoLaterThan, end)
	if err != nil {
		return nil, err
	}

	var filtered []airflow.Dag
	for _, dag := range dags {
		lastRunDate, err := time.Parse(time.RFC3339, dagRuns[dag.DagId].StartDate)
		if err != nil {
			return nil, err
		}
		if (start.Equal(*TIME_BEFORE_TIME) || start.Before(lastRunDate)) && (end.Equal(*TIME_BEFORE_TIME)  || end.After(lastRunDate)) {
			filtered = append(filtered, dag)
		}
	}
	return filtered, nil
}

func parse_time(timeStr string, timeVal *time.Time) error {
	var err error

	if timeStr != "" {
		*timeVal, err = time.Parse(time.RFC3339, timeStr)
		if err != nil {
			return err
		}
	} else {
		timeVal = nil
	}
	return nil
}
