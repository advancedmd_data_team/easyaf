package commands

import (
	"fmt"
	"gitlab.com/advancedmd_data_team/easyaf/internal/airflow"
	"log"
)

type TriggerOpts struct {
	DagIds []string `long:"dag-id" description:"DagId to trigger, can be specified multiple times. Filters are ignored"`
}

func (o TriggerOpts) Execute(args []string) error {

	auth := airflow.InitAuth(EasyAF.Username, EasyAF.Password)
	client := airflow.InitClientData(EasyAF.Host, auth)

	var dagsIds []string
	// If DagIds are specified just trigger those ones
	if len(o.DagIds) > 0 {
		dagsIds = o.DagIds
	} else {
		dags, err := FilterDagsByOpts(client)
		if err != nil {
			return err
		}
		for _, dag := range dags {
			dagsIds = append(dagsIds, dag.DagId)
		}
	}

	// Trigger them dags
	results, err := client.TriggerDags(dagsIds)
	if err != nil {
		log.Fatal(err)
	}
	displayResults(results)
	return nil
}

func displayResults(results []airflow.TriggerResponse) {
	for _, res := range results {
		fmt.Printf("Triggered %s", res.DagId)
	}
}
