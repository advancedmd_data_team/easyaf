package airflow

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"sync"
)

type UpdateBody struct {
	IsPaused bool `json:"is_paused"`
}

// Update data in the dags
//
// As I Read the documents the only the is_paused value is updatable
// So we are going to just use that for now. That is what we want it
// For anyway.
func (c *ClientData) UpdateDags(dagIds []string, pause bool) ([]Dag, error) {
	resultsChannel := make(chan concurrentResponse[Dag], len(dagIds))
	wg := sync.WaitGroup{}

	for _, dagId := range dagIds {
		wg.Add(1)

		go func(d string) {
			defer wg.Done()
			url_path := fmt.Sprintf("/api/v1/dags/%s", d)
			dagRun, err := c.updateDag(url_path, pause)
			if errors.Is(err, FailedToReadResponse) {
				err = DagRunTriggerFailure{d}
			}
			resultsChannel <- concurrentResponse[Dag]{Val: dagRun, Err: err}
		}(dagId)
	}

	go func() {
		wg.Wait()
		close(resultsChannel)
	}()

	var results []Dag
	for res := range resultsChannel {
		switch res.Err.(type) {
		case nil:
			results = append(results, res.Val)
		case DagRunTriggerFailure:
			log.Printf("%s", res.Err)
		default:
			log.Fatal("ERROR MR ROBBINSON: ", res.Err)
			return nil, res.Err
		}
	}
	return results, nil
}

// Update a Single dag
func (c *ClientData) updateDag(url_path string, paused bool) (Dag, error) {
	response := new(Dag)
	body := UpdateBody{IsPaused: paused}
	body_json, err := json.Marshal(body)
	if err != nil {
		return *response, nil
	}

	url := c.baseUrl + url_path
	fmt.Printf("Body is %s", body_json)
	bodyText, err := doRequest("PATCH", url, bytes.NewReader(body_json), c.auth)
	if err != nil {
		return *response, err
	}

	err = json.Unmarshal(bodyText, response)
	if err != nil {
		return *response, err
	}
	if response == nil {
		return *response, FailedToReadResponse
	}
	return *response, nil
}
