// Make requests to airflow
package airflow

import (
	"encoding/json"
	"io"
	"net/http"
	"sync"
)

type listDagRunsResponse struct {
	DagRuns      []DagRun `json:"dag_runs"`
	TotalEntries int      `json:"total_entries"`
}


func (clientData *ClientData) FetchLastDagRuns(dags []Dag) (map[string]DagRun, error) {
	resultsChannel := make(chan concurrentResponse[DagRun], len(dags))
	wg := sync.WaitGroup{}

	for _, dag := range dags {
		wg.Add(1)

		go func(d string) {
			defer wg.Done()
			dagRun, err := clientData.fetchLastDagRun(d)

			resultsChannel <- concurrentResponse[DagRun]{Val: dagRun, Err: err}
		}(dag.DagId)
	}

	go func() {
		wg.Wait()
		close(resultsChannel)
	}()

	var results = make(map[string]DagRun)
	for res := range resultsChannel {
		switch res.Err.(type) {
		case nil:
			results[res.Val.DagId] = res.Val
		case MissingDagRunError:
		default:
			return nil, res.Err
		}
	}
	return results, nil
}

// fetches the last DagRun data for a particular Dag
func (clientData *ClientData) fetchLastDagRun(dagId string) (DagRun, error) {
	var resData listDagRunsResponse
	var dagRun DagRun

	client := &http.Client{}
	url := clientData.baseUrl + "/api/v1/dags/" + dagId + "/dagRuns?limit=1&order_by=-execution_date"
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return dagRun, err
	}

	req.SetBasicAuth(clientData.auth.password, clientData.auth.username)
	req.Header.Set("accept", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		return dagRun, err
	}

	defer resp.Body.Close()
	bodyText, err := io.ReadAll(resp.Body)
	if err != nil {
		return dagRun, err
	}

	err = json.Unmarshal(bodyText, &resData)
	if err != nil {
		return dagRun, err
	}

	if len(resData.DagRuns) == 0 {
		return dagRun, MissingDagRunError{dagId: dagId}
	}
	dagRun = resData.DagRuns[0]
	return dagRun, nil
}

type MissingDagRunError struct {
	dagId string
}

func (e MissingDagRunError) Error() string {
	return "Unable to find DagRun for " + e.dagId
}
