package airflow

import (
	"errors"
	"fmt"
	"io"
	"net/http"
)

// A Useful Type for getting responses from multiple workers
type concurrentResponse[T interface{}] struct {
	Val T
	Err error
}

func doRequest(method string, url string, body io.Reader, auth Auth) ([]byte, error) {
	client := &http.Client{}
	var response []byte

	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, err
	}

	req.SetBasicAuth(auth.password, auth.username)
	req.Header.Set("Content-Type", "application/json")
	resp, err := client.Do(req)

	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	response, err = io.ReadAll(resp.Body)
	if resp.StatusCode >= 400 {
		return nil, errors.New(fmt.Sprintf("%s", response))
	}

	if err != nil {
		return nil, err
	}
	return response, nil

}

type failedToReadResponse struct{}

func (e failedToReadResponse) Error() string {
	return "Failed To Read Response"
}

var FailedToReadResponse = failedToReadResponse{}
