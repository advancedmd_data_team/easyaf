// Make requests to airflow
package airflow

import (
	"encoding/json"
	"fmt"
)

type listDagsResponse struct {
	Dags         []Dag `json:"dags"`
	TotalEntries int   `json:"total_entries"`
}


func (clientData *ClientData) FetchAllDags() ([]Dag, error) {
	requestLimit := 100

	var dags []Dag
	var offset = 0
	// Get first 100
	totalEntries := 100
	for offset = 0; offset < totalEntries; offset += requestLimit {
		url := clientData.baseUrl + "/api/v1/dags?limit=100&only_active=false&offset=" + fmt.Sprint(offset)
		bodyText, err := doRequest("GET", url, nil, clientData.auth)
		if err != nil {
			return nil, err

		}
		var resData listDagsResponse
		err = json.Unmarshal(bodyText, &resData)
		if err != nil {
			return nil, err
		}
		dags = append(dags, resData.Dags...)
		totalEntries = resData.TotalEntries
	}

	return dags, nil
}

