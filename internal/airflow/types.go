package airflow

type Dag struct {
	DagId                       string      `json:"dag_id"`
	DefaultView                 string      `json:"default_view"`
	Description                 string      `json:"description"`
	FileToken                   string      `json:"file_token"`
	Fileloc                     string      `json:"fileloc"`
	HasImportErrors             bool        `json:"has_import_errors"`
	HasTaskConcurrencyLimits    bool        `json:"has_task_concurrency_limits"`
	IsActive                    bool        `json:"is_active"`
	IsPaused                    bool        `json:"is_paused"`
	IsSubdag                    bool        `json:"is_subdag"`
	LastExpired                 string      `json:"last_expired"`
	LastParsedTime              string      `json:"last_parsed_time"`
	LastPickled                 string      `json:"last_pickled"`
	MaxActiveRuns               float64     `json:"max_active_runs"`
	MaxActiveTasks              float64     `json:"max_active_tasks"`
	NextDagrun                  string      `json:"next_dagrun"`
	NextDagrunCreateAfter       string      `json:"next_dagrun_create_after"`
	NextDagrunDataIntervalEnd   string      `json:"next_dagrun_data_interval_end"`
	NextDagrunDataIntervalStart string      `json:"next_dagrun_data_interval_start"`
	Owners                      []string    `json:"owners"`
	PickleId                    string      `json:"pickle_id"`
	RootDag_id                  string      `json:"root_dag_id"`
	ScheduleInterval            interface{} `json:"schedule_interval"`
	SchedulerLock               bool        `json:"scheduler_lock"`
	Tags                        []Tag       `json:"tags"`
	TimetableDescription        string      `json:"timetable_description"`
}

type Tag struct {
	Name string
}

type DagRun struct {
	Conf                   interface{} `json:"conf"`
	DagId                  string      `json:"dag_id"`
	DagRunId               string      `json:"dag_run_id"`
	DataIntervalEnd        string      `json:"data_interval_end"`
	DataIntervalStart      string      `json:"data_interval_start"`
	EndDate                string      `json:"end_date"`
	ExternalTrigger        bool        `json:"external_trigger"`
	LastSchedulingDecision string      `json:"last_scheduling_decision"`
	LogicalDate            string      `json:"logical_date"`
	Note                   string      `json:"note"`
	RunType                string      `json:"run_type"`
	StartDate              string      `json:"start_date"`
	State                  string      `json:"state"`
}
