package airflow

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"sync"
)

type NullableString struct {
	inner string
}

func (o NullableString) MarshalJSON() ([]byte, error) {
	if o.inner == "" {
		return []byte("null"), nil
	}
	return json.Marshal(o.inner)
}

type DagRunTriggerFailure struct {
	dagId string
}

func (e DagRunTriggerFailure) Error() string {
	return "Unable to find DagRun for " + e.dagId
}

type triggerRequestBody struct {
	Conf        map[string]interface{} `json:"conf,omitempty"`
	DagRunId    string                 `json:"dag_run_id,omitempty"`
	LogicalDate string                 `json:"logical_date,omitempty"`
	Note        NullableString         `json:"note"`
}

func DefaultTriggerRequestBody() triggerRequestBody {
	return triggerRequestBody{
		Conf:        make(map[string]interface{}),
		DagRunId:    "",
		LogicalDate: "",
		Note:        NullableString{inner: "Triggered from easyaf"},
	}
}

type TriggerResponse struct {
	Conf                   interface{} `json:"conf"`
	DagId                  string      `json:"dag_id"`
	DagRunId               string      `json:"dag_run_id"`
	DataIntervalEnd        string      `json:"data_interval_end"`
	DataIntervalStart      string      `json:"data_interval_start"`
	EndDate                string      `json:"end_date"`
	ExternalTrigger        bool        `json:"external_trigger"`
	LastSchedulingDecision string      `json:"last_scheduling_decision"`
	LogicalDate            string      `json:"logical_date"`
	Note                   string      `json:"note"`
	RunType                string      `json:"run_type"`
	StartDate              string      `json:"start_date"`
	State                  string      `json:"state"`
}

func (c *ClientData) TriggerDags(dagIds []string) ([]TriggerResponse, error) {
	resultsChannel := make(chan concurrentResponse[TriggerResponse], len(dagIds))
	wg := sync.WaitGroup{}

	for _, dagId := range dagIds {
		wg.Add(1)

		go func(d string) {
			defer wg.Done()
			url_path := fmt.Sprintf("/api/v1/dags/%s/dagRuns", d)
			dagRun, err := c.triggerDag(url_path, DefaultTriggerRequestBody())
			if errors.Is(err, FailedToReadResponse) {
				err = DagRunTriggerFailure{d}
			}
			resultsChannel <- concurrentResponse[TriggerResponse]{Val: dagRun, Err: err}
		}(dagId)
	}

	go func() {
		wg.Wait()
		close(resultsChannel)
	}()

	var results []TriggerResponse
	for res := range resultsChannel {
		switch res.Err.(type) {
		case nil:
			results = append(results, res.Val)
		case DagRunTriggerFailure:
			log.Printf("%s", res.Err)
		default:
			log.Fatal("ERROR MR ROBBINSON: ", res.Err)
			return nil, res.Err
		}
	}
	return results, nil
}

func (c *ClientData) triggerDag(url string, body triggerRequestBody) (TriggerResponse, error) {

	response := new(TriggerResponse)
	body_json, err := json.Marshal(body)
	if err != nil {
		return *response, nil
	}

	url = c.baseUrl + url
	bodyText, err := doRequest("POST", url, bytes.NewReader(body_json), c.auth)
	if err != nil {
		return *response, err
	}

	err = json.Unmarshal(bodyText, response)
	if err != nil {
		return *response, err
	}
	if response == nil {
		return *response, FailedToReadResponse
	}
	return *response, nil
}

