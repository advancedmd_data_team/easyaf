package airflow

type Auth struct {
	username string
	password string
}

func InitAuth(username string, password string) Auth {
	return Auth{
		username: username,
		password: password,
	}
}

type ClientData struct {
	baseUrl string
	auth    Auth
}

func InitClientData(baseUrl string, auth Auth) ClientData {
	return ClientData{
		baseUrl: baseUrl,
		auth:    auth,
	}
}

