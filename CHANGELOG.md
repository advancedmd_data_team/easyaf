# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
### Changed
### Removed

## [0.1.0] - 2024-02-06
Initial Release! Woot!

### Added

- Trigger, List or Pause Pipelines based off of filters or by DagID

See README for more details
### Changed
### Removed

